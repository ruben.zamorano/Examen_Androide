package com.examen.rubenzamorano.examen.Request.GuardarFoto;

/**
 * Created by rubenzamorano on 16/08/17.
 */

public class RQ_GuardarFoto {
    public boolean bError;
    public String cMensaje;
    public String cCodigoError;

    public RQ_GuardarFoto(boolean bError, String cMensaje, String cCodigoError) {
        this.bError = bError;
        this.cMensaje = cMensaje;
        this.cCodigoError = cCodigoError;
    }

    public boolean isbError() {
        return bError;
    }

    public void setbError(boolean bError) {
        this.bError = bError;
    }

    public String getcMensaje() {
        return cMensaje;
    }

    public void setcMensaje(String cMensaje) {
        this.cMensaje = cMensaje;
    }

    public String getcCodigoError() {
        return cCodigoError;
    }

    public void setcCodigoError(String cCodigoError) {
        this.cCodigoError = cCodigoError;
    }
}
