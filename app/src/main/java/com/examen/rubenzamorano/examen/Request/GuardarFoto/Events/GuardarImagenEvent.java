package com.examen.rubenzamorano.examen.Request.GuardarFoto.Events;

import com.examen.rubenzamorano.examen.Request.GuardarFoto.GuardarFotoCallback;
import com.examen.rubenzamorano.examen.Request.GuardarFoto.RQ_GuardarFoto;

/**
 * Created by ruben on 12/02/17.
 */

public class GuardarImagenEvent {

    private RQ_GuardarFoto serverResponse;

    public GuardarImagenEvent(RQ_GuardarFoto serverResponse, final GuardarFotoCallback callback) {
        this.serverResponse = serverResponse;
        callback.onSuccess(serverResponse);
    }

    public RQ_GuardarFoto getServerResponse() {
        return serverResponse;
    }

    public void setServerResponse(RQ_GuardarFoto serverResponse) {
        this.serverResponse = serverResponse;
    }


}
