package com.examen.rubenzamorano.examen.Request.ObtenerImagenes;

import com.examen.rubenzamorano.examen.DTO.DTO_Imagen;

import java.util.List;

/**
 * Created by rubenzamorano on 16/08/17.
 */

public class RQ_ObtenerImagen {
    public boolean bError;
    public String cMensaje;
    public String cCodigoError;
    public List<DTO_Imagen> lstImagenes;

    public RQ_ObtenerImagen(boolean bError, String cMensaje, String cCodigoError, List<DTO_Imagen> lstImagenes) {
        this.bError = bError;
        this.cMensaje = cMensaje;
        this.cCodigoError = cCodigoError;
        this.lstImagenes = lstImagenes;
    }

    public boolean isbError() {
        return bError;
    }

    public void setbError(boolean bError) {
        this.bError = bError;
    }

    public String getcMensaje() {
        return cMensaje;
    }

    public void setcMensaje(String cMensaje) {
        this.cMensaje = cMensaje;
    }

    public String getcCodigoError() {
        return cCodigoError;
    }

    public void setcCodigoError(String cCodigoError) {
        this.cCodigoError = cCodigoError;
    }

    public List<DTO_Imagen> getLstImagenes() {
        return lstImagenes;
    }

    public void setLstImagenes(List<DTO_Imagen> lstImagenes) {
        this.lstImagenes = lstImagenes;
    }
}
