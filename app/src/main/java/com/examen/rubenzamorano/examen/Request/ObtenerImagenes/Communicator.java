package com.examen.rubenzamorano.examen.Request.ObtenerImagenes;


import com.examen.rubenzamorano.examen.Request.ObtenerImagenes.Events.ErrorEvent;
import com.examen.rubenzamorano.examen.Request.ObtenerImagenes.Events.ObtenerImagenesEvent;
import com.squareup.otto.Produce;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rubenzamorano on 16/08/17.
 */

public class Communicator {
    private static final String TAG = "CommunicatorRestaurante";
    private static final String SERVER_URL = "http://tkcore.ddns.net:91";



    public void ObtenerImagenes(String cImei, final ObtenerImagenesCallback callback) {


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(SERVER_URL)
                .build();

        Interface service = retrofit.create(Interface.class);

        Call<RQ_ObtenerImagen> call = service.post(cImei);

        call.enqueue(new Callback<RQ_ObtenerImagen>() {
            @Override
            public void onResponse(Call<RQ_ObtenerImagen> call, Response<RQ_ObtenerImagen> response) {
                // response.isSuccessful() is true if the response code is 2xx
                BusProvider.getInstance().post(new ObtenerImagenesEvent(response.body(), callback));
            }

            @Override
            public void onFailure(Call<RQ_ObtenerImagen> call, Throwable t) {
                // handle execution failures like no internet connectivity
                BusProvider.getInstance().post(new ErrorEvent(-2, t.getMessage()));
            }
        });
    }


    @Produce
    public ObtenerImagenesEvent produceServerEvent(RQ_ObtenerImagen serverResponse, ObtenerImagenesCallback callback) {
        return new ObtenerImagenesEvent(serverResponse, callback);
    }

    @Produce
    public ErrorEvent produceErrorEvent(int errorCode, String errorMsg) {
        return new ErrorEvent(errorCode, errorMsg);
    }
}
