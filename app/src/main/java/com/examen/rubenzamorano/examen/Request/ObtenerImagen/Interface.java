package com.examen.rubenzamorano.examen.Request.ObtenerImagen;




import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by rubenzamorano on 16/08/17.
 */

public interface Interface {

    @FormUrlEncoded
    @POST("/Imagenes.asmx/ObtenerImagen")
    Call<RQ_ObtenerImagen> post(
            @Field("nIdImagen") int nIdImagen
    );



}