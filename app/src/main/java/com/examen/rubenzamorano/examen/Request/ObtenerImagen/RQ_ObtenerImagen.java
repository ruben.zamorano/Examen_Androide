package com.examen.rubenzamorano.examen.Request.ObtenerImagen;

import com.examen.rubenzamorano.examen.DTO.DTO_Imagen;

/**
 * Created by rubenzamorano on 16/08/17.
 */

public class RQ_ObtenerImagen {
    public boolean bError;
    public String cMensaje;
    public String cCodigoError;
    public DTO_Imagen Imagen;

    public RQ_ObtenerImagen(boolean bError, String cMensaje, String cCodigoError, DTO_Imagen imagen) {
        this.bError = bError;
        this.cMensaje = cMensaje;
        this.cCodigoError = cCodigoError;
        Imagen = imagen;
    }

    public boolean isbError() {
        return bError;
    }

    public void setbError(boolean bError) {
        this.bError = bError;
    }

    public String getcMensaje() {
        return cMensaje;
    }

    public void setcMensaje(String cMensaje) {
        this.cMensaje = cMensaje;
    }

    public String getcCodigoError() {
        return cCodigoError;
    }

    public void setcCodigoError(String cCodigoError) {
        this.cCodigoError = cCodigoError;
    }

    public DTO_Imagen getImagen() {
        return Imagen;
    }

    public void setImagen(DTO_Imagen imagen) {
        Imagen = imagen;
    }
}
