package com.examen.rubenzamorano.examen.Request.ObtenerImagen.Events;

import com.examen.rubenzamorano.examen.Request.ObtenerImagen.ObtenerImagenCallback;
import com.examen.rubenzamorano.examen.Request.ObtenerImagen.RQ_ObtenerImagen;

/**
 * Created by ruben on 12/02/17.
 */

public class ObtenerImagenEvent {

    private RQ_ObtenerImagen serverResponse;

    public ObtenerImagenEvent(RQ_ObtenerImagen serverResponse, final ObtenerImagenCallback callback) {
        this.serverResponse = serverResponse;
        callback.onSuccess(serverResponse);
    }

    public RQ_ObtenerImagen getServerResponse() {
        return serverResponse;
    }

    public void setServerResponse(RQ_ObtenerImagen serverResponse) {
        this.serverResponse = serverResponse;
    }


}
