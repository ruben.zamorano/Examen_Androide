package com.examen.rubenzamorano.examen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.examen.rubenzamorano.examen.Activities.ImagenActivity;
import com.examen.rubenzamorano.examen.DTO.DTO_Imagen;
import com.examen.rubenzamorano.examen.R;

import java.util.List;


/**
 * Adaptador para mostrar los items de las fotos que ha tomado el telefono
 */
public class AdaptadorImagenes extends RecyclerView.Adapter<AdaptadorImagenes.ViewHolder> {


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView cTextoImagen;
        public LinearLayout item_img;



        public ViewHolder(View v) {
            super(v);
            cTextoImagen =  v.findViewById(R.id.tv_imagen);
            item_img = v.findViewById(R.id.item_img);

        }
    }

    private static final String TAG = "AdaptadorImagenes";
    private Context mContext;
    public List<DTO_Imagen> items;

    public AdaptadorImagenes(Context context, List<DTO_Imagen> lst)
    {
        this.mContext = context;
        this.items = lst;

    }



    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        final DTO_Imagen item = items.get(i);

        viewHolder.cTextoImagen.setText("Imagen " + i);
        viewHolder.item_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(new Intent(view.getContext(), ImagenActivity.class));

                intent.putExtra("nIdImagen",item.nIdImagen);
                view.getContext().startActivity(intent);

            }
        });









    }




}