package com.examen.rubenzamorano.examen.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.examen.rubenzamorano.examen.Helpers.GPSTracker;
import com.examen.rubenzamorano.examen.R;
import com.examen.rubenzamorano.examen.Request.GuardarFoto.Communicator;
import com.examen.rubenzamorano.examen.Request.GuardarFoto.GuardarFotoCallback;
import com.examen.rubenzamorano.examen.Request.GuardarFoto.RQ_GuardarFoto;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;

public class MainActivity extends AppCompatActivity implements GuardarFotoCallback {

    private static final String TAG = "MainActivity";
    public static final int CAPTURA_IMAGEN = 1000;
    private static final int REQUEST_READ_CONTACTS = 0;

    public Communicator communicator = new Communicator();
    public LinearLayout layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        layout = (LinearLayout)findViewById(R.id.Contenedor_main);

        Button btn_TomarFoto = (Button)findViewById(R.id.btn_TomarFoto);
        btn_TomarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



        if (ValidarPermisos()) {
            Log.d(TAG,"ClickFoto");
            Intent imageIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(imageIntent, CAPTURA_IMAGEN);
        }





            }
        });

        Button btn_MostrarFotos = (Button)findViewById(R.id.btn_MostrarFotos);
        btn_MostrarFotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MostrarImagenesActivity.class);
                startActivity(intent);
            }
        });






    }



    private boolean ValidarPermisos() {
        boolean isValid = false;
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            isValid = true;
        }
        else
        {
            List<String> lstPermisos = new ArrayList<>();
            if (checkSelfPermission(ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                lstPermisos.add(ACCESS_FINE_LOCATION);
            }
            if (checkSelfPermission(CAMERA) != PackageManager.PERMISSION_GRANTED) {
                lstPermisos.add(CAMERA);
            }
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                lstPermisos.add(Manifest.permission.READ_PHONE_STATE);
            }

            if(lstPermisos.size() > 0)
            {

                requestPermissions(lstPermisos.toArray(new String[0]), REQUEST_READ_CONTACTS);
                return false;
            }

        }


        if (!manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            isValid = false;
            Toast toast = Toast.makeText(this, "Favor de encender el GPS", Toast.LENGTH_LONG);
            toast.show();
        }
        else
        {
            isValid = true;
        }
        return isValid;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String cMensaje =  "";
        if (resultCode == RESULT_OK)
        {

            switch (requestCode)
            {
                case CAPTURA_IMAGEN:


                    //Convertimos la imagen a bitmap
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    //Creamos un arreglo de bytes de salida (donde se guarda)
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    // comprimimos la imagen (en bitmap) y la almacenamos en el output
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    //el output se almacena en un arreglo de bytes
                    byte[] b = baos.toByteArray();
                    //Tenemos la imagne en base64
                    String encodedImage = Base64.encodeToString(b, Base64.NO_WRAP);

                    Log.d(TAG,"Imagen: " + encodedImage);
                    TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
                    String cImei = telephonyManager.getDeviceId();
                    Log.d(TAG,"IMEI: " + cImei);

                    GPSTracker gps = new GPSTracker(this);
                    gps.getLatitude();
                    gps.getLongitude();
                    Log.d(TAG,"Coordenadas: " + gps.getLatitude() + gps.getLongitude());

                    communicator.GuardarFoto(cImei,encodedImage, gps.getLatitude() + "," + gps.getLongitude(),this);

                    break;


            }

        }
        else
        {
            //error

        }



    }


    @Override
    public void onSuccess(RQ_GuardarFoto result) {
            Log.d(TAG,"onSuccess:" + result.bError);
    }
}
