package com.examen.rubenzamorano.examen.Request.GuardarFoto;




import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by rubenzamorano on 16/08/17.
 */

public interface Interface {

    @FormUrlEncoded
    @POST("/Imagenes.asmx/GuardarImagen")
    Call<RQ_GuardarFoto> post(
            @Field("cImei") String cImei,
            @Field("cFoto") String cFoto,
            @Field("cCoordenadas") String cCoordenadas

    );



}