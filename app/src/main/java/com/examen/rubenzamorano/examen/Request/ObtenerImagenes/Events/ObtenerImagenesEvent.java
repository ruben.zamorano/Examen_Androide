package com.examen.rubenzamorano.examen.Request.ObtenerImagenes.Events;

import com.examen.rubenzamorano.examen.Request.ObtenerImagenes.ObtenerImagenesCallback;
import com.examen.rubenzamorano.examen.Request.ObtenerImagenes.RQ_ObtenerImagen;

/**
 * Created by ruben on 12/02/17.
 */

public class ObtenerImagenesEvent {

    private RQ_ObtenerImagen serverResponse;

    public ObtenerImagenesEvent(RQ_ObtenerImagen serverResponse, final ObtenerImagenesCallback callback) {
        this.serverResponse = serverResponse;
        callback.onSuccess(serverResponse);
    }

    public RQ_ObtenerImagen getServerResponse() {
        return serverResponse;
    }

    public void setServerResponse(RQ_ObtenerImagen serverResponse) {
        this.serverResponse = serverResponse;
    }


}
