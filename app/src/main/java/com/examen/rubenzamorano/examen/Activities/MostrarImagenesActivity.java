package com.examen.rubenzamorano.examen.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;

import com.examen.rubenzamorano.examen.Adapters.AdaptadorImagenes;
import com.examen.rubenzamorano.examen.DTO.DTO_Imagen;
import com.examen.rubenzamorano.examen.R;
import com.examen.rubenzamorano.examen.Request.ObtenerImagenes.Communicator;
import com.examen.rubenzamorano.examen.Request.ObtenerImagenes.ObtenerImagenesCallback;
import com.examen.rubenzamorano.examen.Request.ObtenerImagenes.RQ_ObtenerImagen;

import java.util.List;


public class MostrarImagenesActivity extends AppCompatActivity implements ObtenerImagenesCallback {

    final Communicator communicator = new Communicator();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_imagenes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        communicator.ObtenerImagenes(ObtenerIMEI(),this);


    }


    public String ObtenerIMEI()
    {
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String cImei = telephonyManager.getDeviceId();
        return  cImei;

    }

    public void InicializarRecycler(List<DTO_Imagen> lstImagenes)
    {
        LinearLayoutManager lm = new LinearLayoutManager(this);
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recycler_img);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(new AdaptadorImagenes(this,lstImagenes));
        recyclerView.setNestedScrollingEnabled(false);

    }


    @Override
    public void onSuccess(RQ_ObtenerImagen result) {
        InicializarRecycler(result.lstImagenes);
    }
}
