package com.examen.rubenzamorano.examen.Request.GuardarFoto;


import com.examen.rubenzamorano.examen.Request.GuardarFoto.Events.ErrorEvent;
import com.examen.rubenzamorano.examen.Request.GuardarFoto.Events.GuardarImagenEvent;
import com.google.gson.Gson;
import com.squareup.otto.Produce;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rubenzamorano on 16/08/17.
 */
public class Communicator {
    private static final String TAG = "CommunicatorGuardarFoto";
    private static final String SERVER_URL = "http://tkcore.ddns.net:91";



    public void GuardarFoto(String cImei, String cFoto, String cCoordenadas, final GuardarFotoCallback callback) {


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(SERVER_URL)
                .build();

        Interface service = retrofit.create(Interface.class);

        Call<RQ_GuardarFoto> call = service.post(cImei,cFoto,cCoordenadas);

        call.enqueue(new Callback<RQ_GuardarFoto>() {
            @Override
            public void onResponse(Call<RQ_GuardarFoto> call, Response<RQ_GuardarFoto> response) {
                // response.isSuccessful() is true if the response code is 2xx
                BusProvider.getInstance().post(new GuardarImagenEvent(response.body(), callback));
            }

            @Override
            public void onFailure(Call<RQ_GuardarFoto> call, Throwable t) {
                // handle execution failures like no internet connectivity
                BusProvider.getInstance().post(new ErrorEvent(-2, t.getMessage()));
            }
        });
    }


    @Produce
    public GuardarImagenEvent produceServerEvent(RQ_GuardarFoto serverResponse, GuardarFotoCallback callback) {
        return new GuardarImagenEvent(serverResponse, callback);
    }

    @Produce
    public ErrorEvent produceErrorEvent(int errorCode, String errorMsg) {
        return new ErrorEvent(errorCode, errorMsg);
    }
}
