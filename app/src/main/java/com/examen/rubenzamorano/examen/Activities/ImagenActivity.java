package com.examen.rubenzamorano.examen.Activities;


import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;

import com.examen.rubenzamorano.examen.R;
import com.examen.rubenzamorano.examen.Request.ObtenerImagen.Communicator;
import com.examen.rubenzamorano.examen.Request.ObtenerImagen.ObtenerImagenCallback;
import com.examen.rubenzamorano.examen.Request.ObtenerImagen.RQ_ObtenerImagen;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

public class ImagenActivity extends AppCompatActivity implements ObtenerImagenCallback, OnMapReadyCallback {

    private static final String TAG = "ImagenActivity";
    final Gson gson = new Gson();

    public CollapsingToolbarLayout toolbarLayout;
    public Toolbar mtoolbar;
    private GoogleMap mMap;
    public double nLatitud = 0;
    public double nLongitud = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagen);
        mtoolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mtoolbar);
        setToolbar("La imagen fue tomada aqui:");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            int nIdImagen = bundle.getInt("nIdImagen");
            final Communicator communicator = new Communicator();
            communicator.ObtenerImagen(nIdImagen,this);


        }


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        if(nLatitud > 0)
        {
            setMarker(nLatitud,nLongitud);
        }


        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }









    }

    @Override
    public void onSuccess(RQ_ObtenerImagen result) {

        byte[] decodedString = Base64.decode(result.getImagen().cFoto, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        BitmapDrawable ob = new BitmapDrawable(getResources(), decodedByte);

        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            toolbarLayout.setBackgroundDrawable(ob);
        } else {
            toolbarLayout.setBackground(ob);
        }

        nLatitud = result.Imagen.nLatitud;
        nLongitud = result.Imagen.nLongitud;

        setMarker(result.Imagen.nLatitud,result.Imagen.nLongitud);
    }

    protected void setToolbar(String cTitulo) {
        mtoolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mtoolbar != null) {
            mtoolbar.setNavigationContentDescription(cTitulo);

            setSupportActionBar(mtoolbar);
            getSupportActionBar().setTitle(cTitulo);
        }
    }

    public void setMarker(double nLat, double nLng)
    {

        if(mMap != null)
        {
            LatLng sydney = new LatLng(nLat, nLng);

            //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,18.0f));

            CameraUpdate center =
                    CameraUpdateFactory.newLatLng(sydney);
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(18.0f);

            mMap.moveCamera(center);
            mMap.animateCamera(zoom);

            //mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mMap.addMarker(new MarkerOptions().position(sydney).title(""));

        }



    }
}
