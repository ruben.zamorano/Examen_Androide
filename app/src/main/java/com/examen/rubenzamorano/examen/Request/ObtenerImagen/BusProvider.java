package com.examen.rubenzamorano.examen.Request.ObtenerImagen;

/**
 * Created by rubenzamorano on 16/08/17.
 */

import com.squareup.otto.Bus;

public class BusProvider {

    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }

    public BusProvider() {
    }
}

