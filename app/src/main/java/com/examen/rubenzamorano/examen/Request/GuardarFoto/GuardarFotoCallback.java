package com.examen.rubenzamorano.examen.Request.GuardarFoto;



/**
 * Created by ruben on 02/02/17.
 */

public interface GuardarFotoCallback {

    void onSuccess(RQ_GuardarFoto result);
}
