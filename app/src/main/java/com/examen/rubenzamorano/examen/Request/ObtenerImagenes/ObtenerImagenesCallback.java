package com.examen.rubenzamorano.examen.Request.ObtenerImagenes;


/**
 * Created by ruben on 02/02/17.
 */

public interface ObtenerImagenesCallback {

    void onSuccess(RQ_ObtenerImagen result);
}
