package com.examen.rubenzamorano.examen.Request.ObtenerImagen;


/**
 * Created by ruben on 02/02/17.
 */

public interface ObtenerImagenCallback {

    void onSuccess(RQ_ObtenerImagen result);
}
