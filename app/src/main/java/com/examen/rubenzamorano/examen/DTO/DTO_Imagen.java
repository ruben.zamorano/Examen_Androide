package com.examen.rubenzamorano.examen.DTO;

/**
 * Created by rubenzamorano on 16/08/17.
 */

public class DTO_Imagen {
    public String cFoto;
    public float nLatitud;
    public float nLongitud;
    public int nIdImagen;

    public DTO_Imagen(String cFoto, float nLatitud, float nLongitud, int nIdImagen) {
        this.cFoto = cFoto;
        this.nLatitud = nLatitud;
        this.nLongitud = nLongitud;
        this.nIdImagen = nIdImagen;
    }

    public String getcFoto() {
        return cFoto;
    }

    public void setcFoto(String cFoto) {
        this.cFoto = cFoto;
    }

    public float getnLatitud() {
        return nLatitud;
    }

    public void setnLatitud(float nLatitud) {
        this.nLatitud = nLatitud;
    }

    public float getnLongitud() {
        return nLongitud;
    }

    public void setnLongitud(float nLongitud) {
        this.nLongitud = nLongitud;
    }

    public int getnIdImagen() {
        return nIdImagen;
    }

    public void setnIdImagen(int nIdImagen) {
        this.nIdImagen = nIdImagen;
    }
}
